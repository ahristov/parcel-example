# Parcel Example Project

Contains example setup project using [Parcel](https://parceljs.org/) as a build tool.

## Directories

The project contains the following directory structure:

    .babelrc              - Babel configuration
    .gitignore            - Git ignore
    package.json          - Npm config file
    package-lock.json     - Npm lock file
    README.md             - This file
    src                   - Directory with source code
    public                - Directory with compiled assets

## Npm commands

To start using, install the dependencies:

```bash
npm install
```

To run development mode, run:

```bash
npm run develop
```

To compile the code, run:

```bash
npm run build
```
