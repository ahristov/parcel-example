const getFullName = (firstName, lastName) => {
  firstName = firstName ?? 'John';
  lastName = lastName ?? 'Doe';

  return `${firstName} ${lastName}`;
}

export { getFullName };