import { getFullName } from './utils/names';
import '../styles/index.scss';

const fullName = getFullName();
const helloWorldElement = document.getElementById('hello-world');

helloWorldElement && (helloWorldElement.innerText = `Hello ${fullName}!`);
